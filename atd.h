#ifndef __ATD_H_
#define __ATD_H_

#include <pthread.h>
#include <stdarg.h>
#include <stdlib.h>

#define ATC_HANDLER(F) int F(char *atd_buf, int atd_buf_sz, void *ex_data)

/******************/
/* ATD INIT FUNCS */
/******************/
int atd_init();
int atd_destroy();

/********************/
/* ATD CLIENT FUNCS */
/********************/
int atd_client_init(const char *path);
/* Loops + Handlers for IO */
int atd_listen_mode(void *ex_data, ATC_HANDLER((*async_handler)));
int atd_cmd_mode   (void *ex_data, ATC_HANDLER((*req_input_handler)), ATC_HANDLER((*req_output_handler)));
/* One-time uses */
int atdcmd(int sock, char *rsp, const int buffsz, const char *fmt, ...);
int atdlsn(int sock, char *rsp, const int buffsz, const int timeout_ms);

#endif // __ATD_H_
