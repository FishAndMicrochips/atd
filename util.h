#ifndef __UTIL_H_
#define __UTIL_H_


/*
 * Useful arguent functions by 20h
 * use main(int argc, char *argv[])
 */
#define ARGBEGIN \
        for (argv++, argc--; argv[0] && argv[0][0] == '-' && argv[0][1]; argc--, argv++) {\
            char argc_;\
            char **argv_;\
            int brk_;\
            if (argv[0][1] == '-' && argv[0][2] == '\0') {\
                argv++;\
                argc--;\
                break;\
            }\
            for (brk_ = 0, argv[0]++, argv_ = argv; argv[0][0] && !brk_; argv[0]++) {\
                if (argv_ != argv)\
                    break;\
                argc_ = argv[0][0];\
                switch (argc_)
#define ARGEND \
            }\
        }

#define ARGC()      argc_

#define GETARG_OR_DO(FUNC) \
            ((argv[0][1] == '\0' && argv[1] == NULL)\
                ? ((FUNC), abort(), (char *)0)\
                : (brk_ = 1, (argv[0][1] != '\0')\
                    ? (&argv[0][1])\
                    : (argc--, argv++, argv[0])))

#define GETARG()\
            ((argv[0][1] == '\0' && argv[1] == NULL)\
                ? (char *)0\
                : (brk_ = 1, (argv[0][1] != '\0')\
                                 ? (&argv[0][1])\
                                 : (argc--, argv++, argv[0])))


#define LEN(X) (sizeof(X) / sizeof(*(X)))

#define BETWEEN(N, A, B) ((N) >= (A) && (N) <= (B))

#define DO_OR_DIE(FUN) if ((FUN) < 0) die("%s: %s\n", #FUN, strerror(errno))


#endif // __UTIL_H_
