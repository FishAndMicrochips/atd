# atd Version
VERSION = 0.5

# Customize these to fit your system:
LIBS = -lpthread

# paths
PREFIX = /usr/local

CPPFLAGS = -D_DEFAULT_SOURCE -D_BSD_SOURCE -D_POSIX_C_SOURCE=2 -DVERSION=\"${VERSION}\"
CFLAGS = -std=c99 -pedantic -Wall -Wno-deprecated-declarations -O2 ${INCS} ${CPPFLAGS} -fPIC
LDFLAGS = ${LIBS}

CC = cc
