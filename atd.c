#include <errno.h>
#include <fcntl.h>
#include <locale.h>
#include <poll.h>
#include <pthread.h>
#include <regex.h>
#include <signal.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <syslog.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/un.h>
#include <termios.h>
#include <unistd.h>

#include "config.h"
#include "atd.h"

/* ASYNC MODE */
static int async_opcon(struct pollfd *clients, int maxnclients, int srv_fd, struct sockaddr *addr, socklen_t addrlen);
static void * asrv_sockmux(void *path_void);

/* REQUEST MODE */
static void * rsrv_connmain(void *client_void);
static void * rsrv_sockmux(void *path_void);

/* LOGGING */
static pthread_mutex_t log_mx;
static int log_mx_en = 0;
static int mxvsyslog(int priority, const char *fmt, va_list vp);
static int warn(const char *fmt, ...);
static int notice(const char *fmt, ...);

/* MODEM IO */
static pthread_mutex_t modem_mx;
static int modem = 0;
static int modem_snprintf(char *rsp, const unsigned int bufsz, const unsigned int timeout_ms, const char *fmt, ...);

/* THREADING */
static pthread_t reqth, asyncth;

/* FILE DESCRIPTOR IO */
/*** General Purpose file descriptor IO ***/
static int fd_timeout_read(const int fd, char *buf, size_t buffsz, unsigned int timeout_ms);
static int fdnvprintf(const int fd, const int sz, const char *fmt, va_list va);
static int fdnprintf(const int fd, const int sz, const char *fmt, ...);
/*** Apply a printf logic to requesting info from a file descriptor ***/
static int fdreqnvprintf(const int fd,
                         const unsigned int timeout_ms,
                         const size_t rspsz, char *rsp,
                         const size_t reqsz, const char *req,
                         va_list va);
static int fdreqnprintf(const int fd,
                        const unsigned int timeout_ms,
                        const size_t rspsz, char *rsp,
                        const size_t reqsz, const char *req,
                        ...);

static int sock_server_setup(const char *path);

/*******************/
/* SOCKET HANDLING */
/*******************/

static int
sock_server_setup(const char *path)
{
	int fd;
        struct sockaddr_un addr;
        socklen_t addrlen;

        /* Set up address */
        addr.sun_family = AF_UNIX;
        strncpy(addr.sun_path, path, sizeof(addr.sun_path) - 1);
        addrlen = sizeof addr;

        /* Open server */
        if ((fd = socket(AF_UNIX, SOCK_STREAM, 0)) < 0) 
		return warn("Unable to set up socket %s", path);

        if (bind(fd, (struct sockaddr *) &addr, addrlen) < 0) {
		close(fd);
		return warn("Unable to bind to socket %s", path);
	}

	/* Listen for any inputs on the server */
        if (listen(fd, MAX_NUM_CLIENTS) < 0) {
		close(fd);
		return warn("Unable to listen to socket %s", path);
	}
	return fd;
}

/**************/
/* ASYNC MODE */
/**************/

static void *
asrv_sockmux(void *path_void)
{
        int status, bytes, i, pollready;
        char tbuf[BUFFSZ] = {0}, *path;
        struct sockaddr_un addr;
        socklen_t addrlen;
	struct pollfd fds[MAX_NUM_CLIENTS + 1], *clients, *server;
	const int nfds = MAX_NUM_CLIENTS + 1;

        notice("async_main started\n");
	status = 0;

	/* Initialize file descriptors */
	memset(fds, 0, nfds*sizeof(struct pollfd));
	server = fds;
	clients = fds + 1;

        /* Set up address */
	path = (char*) path_void;
        addr.sun_family = AF_UNIX;
        strncpy(addr.sun_path, path, sizeof(addr.sun_path) - 1);
        addrlen = sizeof addr;

	server->events = POLLIN;
	server->fd = sock_server_setup((char*) path_void);

	bytes = 0;
        while (status >= 0) {
		/* Get any modem data */

	        pthread_mutex_lock(&modem_mx);
       		bytes = fd_timeout_read(modem, tbuf, BUFFSZ, MODEM_TIMEOUT_MS/5);
        	pthread_mutex_unlock(&modem_mx);

                if (bytes < 0) {
			status = warn("Failed to read from modem");	
			close(server->fd);
       			pthread_exit(&status);
		} 

		/* If we have data, print it to all current clients. */
		if (bytes)
			for (i = 0; i < MAX_NUM_CLIENTS; ++i)
				if (clients[i].fd)
                			fdnprintf(clients[i].fd, BUFFSZ, "%s\n", tbuf); 
		/* Now, we handle clients */
		if ((pollready = poll(fds, nfds, MODEM_TIMEOUT_MS)) < 0 && errno != EINTR) {
			status = warn("Failed to poll file descriptors.");
			close(server->fd);
       			pthread_exit(&status);
		}

		/* Check the server for any incoming connections*/
		if (server->revents & POLLIN) {
			server->revents &= ~POLLIN;
			if (async_opcon(clients, MAX_NUM_CLIENTS, server->fd, (struct sockaddr *) &addr, addrlen) < 0) {
				status = warn("Failed to accept new connection.");
				close(server->fd);
       				pthread_exit(&status);
			}
		}
		/* Check the clients for if they have disconnected. */
		for (i = 0; i < MAX_NUM_CLIENTS; ++i)
			if (clients[i].fd && clients[i].revents) {
				clients[i].revents = 0;
				/* Get a heartbeat from each client */
				if ((bytes = fd_timeout_read(clients[i].fd, tbuf, BUFFSZ, MODEM_TIMEOUT_MS/5)) < 0) {
					status = warn("Failed to get heartbeat from file descriptor %d", clients[i].fd);
					close(server->fd);
       					pthread_exit(&status);
				}
				if (!bytes) {
					notice("Closing file descriptor %d", clients[i].fd);
					close(clients[i].fd);
					clients[i].fd = 0;
					clients[i].events = 0;
				}
			}
	}
	close(server->fd);
        pthread_exit(&status);
}

static int
async_opcon(struct pollfd *clients, int maxnclients, int srv_fd, struct sockaddr *addr, socklen_t addrlen)
{
	int i;
	/* Find a free spot in the client list */
	for (i = 0; i < maxnclients; ++i)
		if (!clients[i].fd)
			break;
	if (i >= maxnclients)
		return 0;
	/* Now that we've found a free spot, accept the connection */
	if ((clients[i].fd = accept(srv_fd, addr, &addrlen)) < 0)
		return -1;
	clients[i].events = POLLIN;
	clients[i].revents = 0;	
	notice("new client on file descriptor %d", clients[i]);
	return i;
}

/****************/
/* REQUEST MODE */
/****************/

static int
send_sms(int client)
{
        int bytes;
        char textbuf[BUFFSZ] = {0}, tbuf[BUFFSZ] = {0}, *s;
        const char sms_term = 26;

        while ((bytes = read(client, textbuf, BUFFSZ)) > 0) {
		if (!strcmp(textbuf, "\n"))
			break;
                /* Clean the string */
                while ((s = strchr(textbuf, '\r')))
                        *s = '\n';
                /* Append each line to the text buffer */
                modem_snprintf(tbuf, BUFFSZ, MODEM_TIMEOUT_MS, "%s", textbuf);
                fdnprintf(client, BUFFSZ, "%s\n", tbuf);

                memset(textbuf, 0, sizeof textbuf);
        }
        /* Finally, send the text */
        modem_snprintf(tbuf, BUFFSZ, MODEM_TIMEOUT_MS * 20, "%c\r\n", sms_term);
        fdnprintf(client, BUFFSZ, "%s\n", tbuf);
        if (bytes < 0)
                return warn("Failed to read from file descriptor %d", client);
        return 0;
}

static void *
rsrv_connmain(void *client_void)
{
        int client, bytes, status;
        static int timeout_ms = MODEM_TIMEOUT_MS;
        char rbuf[BUFFSZ] = {0},           /* Buffer to receive from the client */
	     tbuf[BUFFSZ] = {0},           /* Buffer to transmit to the client */
	     *s = NULL;
	//char cmdbuf[BUFFSZ] = {0},         /* Pool of data containing command line arguments */
	//     *argv[MAX_NUM_ARGS] = {NULL}, /* Array of pointers containing command line arguments contained in cmdbuf */
	//int argc;

        status = 0;
        client = *(int *) client_void;
        notice("req_main started with client file descriptor %d", client);

	/* Receive Copyright Message */
        if (fdnprintf(client, BUFFSZ, "%s\n", COPYRIGHT_MESSAGE) < 0) {
                status = warn("Failed to receive Copyright Message");
                pthread_exit(&status);
        }

	///* Receive Command line argument vector */
        //if (read(client, cmdbuf, BUFFSZ) < 0) {
        //        status = warn("Failed to receive command line arguments");
        //        pthread_exit(&status);
        //}
        //for (argc = 0, s = strtok(cmdbuf, " "); s != NULL && argc < MAX_NUM_ARGS; s = strtok(NULL, " "))
        //        argv[argc++] = s;
        
	/* Begin main REPL */
        while (1) {
		/* Get the data from the client */
		memset(rbuf, 0, BUFFSZ * sizeof(char));
		memset(tbuf, 0, BUFFSZ * sizeof(char));
		if ((bytes = read(client, rbuf, BUFFSZ)) < 0) {
                	warn("Failed to read from file descriptor %d", client);
                	status = -1;
			break;
        	}
		if (!bytes)
			continue;

                if (!strncmp(rbuf, "quit", 4))
                        break;

		/* Clean the input of any newlines */
		while ((s = strstr(rbuf, "\n")))
			*s = '\r';
		/* Send the command to the modem, then send the response to the client */	
        	pthread_mutex_lock(&modem_mx);
                modem_snprintf(tbuf, BUFFSZ, timeout_ms, "%s\r\n", rbuf);
                fdnprintf(client, BUFFSZ, "%s\n", tbuf);

                /* Exceptions to the standard command loop begin here */
                if (strstr(tbuf, ">") && !strncmp(rbuf, "AT+CMGS", 7))
                        send_sms(client);
        	pthread_mutex_unlock(&modem_mx);
        }
        notice("Closing client on File Descriptor %d", client);
        close(client);
        pthread_exit(&status);
}

static void *
rsrv_sockmux(void *path_void)
{
        int status, server, client_accept, *new_client;
        struct sockaddr_un addr;
        socklen_t addrlen;
        pthread_t client_thread;

        notice("req_main started");
	status = 0;

	server = sock_server_setup((char*) path_void);

	/* Assign a thread to every new client - REPLS can be operated independently. */
        while ((client_accept = accept(server, (struct sockaddr *) &addr, &addrlen)) > 0) {
                new_client = malloc(sizeof(int));
                *new_client = client_accept;
                if (pthread_create(&client_thread, NULL, rsrv_connmain, (void*) new_client) < 0) {
                        status = warn("Failed to create thread for Client on File Descriptor %d", new_client);
			break;
                }
        }
        if (client_accept < 0) {
                status = warn("Failed to accept connection");
        }
        close(server);
        pthread_exit(&status);
}

/**********************/
/* FILE DESCRIPTOR IO */
/**********************/

static int
fd_timeout_read(int fd, char *buf, size_t buffsz, unsigned int timeout_ms)
{
        struct pollfd pfd;
        int bytes, chunksz, pollst;

        pfd.fd = fd;
        pfd.events = POLLIN;

        memset(buf, 0, buffsz * sizeof(char));

        for (bytes = 0; (pollst = poll(&pfd, 1, timeout_ms)) > 0; bytes += chunksz, buffsz -= chunksz) {
		if (pollst < 0)
			return -1;
                if ((chunksz = read(fd, buf + bytes, buffsz)) < 0)
                        return -1;
		if (!chunksz || buffsz <= 0) /* Full Buffer */
			break;
                pfd.revents = 0;
                timeout_ms = 50;
        }

        /* Don't forget the null terminator! */
        buf[bytes] = '\0';
        return bytes;
}

static int
fdnvprintf(const int fd, const int sz, const char *fmt, va_list va)
{
        int n, b;
        char *buf;
        if (!fmt)
                return 0;
        buf = calloc(sz, sizeof(char));
        n = vsnprintf(buf, sz, fmt, va);
        if ((b = write(fd, buf, sz)) < 0) {
                warn("Unable to write data to file descriptor %d", fd);
                return -1;
        }
        if (b < n) {
                warn("Unable to write all data to file descriptor %d", fd);
                return -1;
        }
        free(buf);
        return b;
}

static int
fdnprintf(const int fd, const int sz, const char *fmt, ...)
{
        int n;
        va_list va;
        va_start(va, fmt);
        n = fdnvprintf(fd, sz, fmt, va);
        va_end(va);
        return n;
}


static int
fdreqnvprintf(const int fd,
              const unsigned int timeout_ms,
              const size_t rspsz, char *rsp,
              const size_t reqsz, const char *req,
              va_list va)
{
        /* Send request */
        size_t bytes;
        bytes = fdnvprintf(fd, reqsz, req, va);
        if (bytes < 0 || bytes < reqsz) {
                strncpy(rsp, "ERROR\n", rspsz);
                return -1;
        }
        tcflush(fd, TCIFLUSH);
        /* Get the response */
        if (!rsp)
                return bytes;
        memset(rsp, 0, rspsz * sizeof(char));
        bytes = fd_timeout_read(fd, rsp, rspsz-1, timeout_ms);
        if (!bytes)
                strncpy(rsp, "TIMEOUT\n", rspsz);
        return bytes;
}

static int
fdreqnprintf(const int fd,
             const unsigned int timeout_ms,
             const size_t rspsz, char *rsp,
             const size_t reqsz, const char *req,
             ...)
{
        int bytes;
        va_list va;
        va_start(va, req);
        bytes = fdreqnvprintf(fd, timeout_ms, rspsz, rsp, reqsz, req, va);
        va_end(va);
        return bytes;
}

/***********/
/* LOGGING */
/***********/

/* NOTE: This may need to be reinterpreted when using things like ncurses */

static int
mxvsyslog(int priority, const char *fmt, va_list vp)
{
        FILE *os;
        int n;
        if (log_mx_en)
                pthread_mutex_lock(&log_mx);
        switch(priority) {
        case LOG_ALERT: /* FALLTHROUGH */
        case LOG_CRIT:
        case LOG_ERR:
        case LOG_WARNING:
                os = stderr;
                break;
        default:
                os = stdout;
        }
        n = vfprintf(os, fmt, vp);
        if (os == stderr)
                n = (n + fprintf(os, " | Error string: %s\n", strerror(errno))) * -1;
        else
                n += fprintf(os, "\n");
        if (log_mx_en)
                pthread_mutex_unlock(&log_mx);
        return n;
}

static int
notice(const char *fmt, ...)
{
        int n;
        va_list vp;
        va_start(vp, fmt);
        n = MXVSYSLOG(LOG_NOTICE, fmt, vp);
        va_end(vp);
        return n;
}

static int
warn(const char *fmt, ...)
{
        int n;
        va_list vp;
        va_start(vp, fmt);
        n = MXVSYSLOG(LOG_WARNING, fmt, vp);
        va_end(vp);
        return n;
}


/************/
/* MODEM IO */
/************/

static int
modem_snprintf(char *rsp, const unsigned int bufsz, const unsigned int timeout_ms, const char *fmt, ...)
{
        va_list va;
        int bytes;
        char *req;

        req = calloc(bufsz, sizeof(char));

        /* Format the string */
        va_start(va, fmt);
        bytes = vsnprintf(req, bufsz, fmt, va);
        va_end(va);

        /* Write the AT command and get the response */
        //pthread_mutex_lock(&modem_mx);
        fdreqnprintf(modem, timeout_ms, bufsz, rsp, strlen(req) + 1, "%s", req);
        //pthread_mutex_unlock(&modem_mx);

        free(req);
        return bytes;
}

/*************************/
/* INTERFACING FUNCTIONS */
/*************************/

static void
atd_sigint()
{
        exit(atd_destroy());
}


int
atd_init()
{
        const char *ttypath = "/dev/ttyS0";
        struct termios tconf;
        const struct init_cmd *cp;
        char rbuf[BUFFSZ] = {0};

        // Create directory for req and async threads
        mkdir(SOCK_PATH, 0760);

        /* Setup Logging */
        if (pthread_mutex_init(&log_mx, NULL) < 0)
                return warn("Failed to initialize logging mutex");
        setlocale(LC_ALL, "");
        log_mx_en = 1;

        /* Setup Modem */
        modem = open(ttypath, O_RDWR | O_NOCTTY | O_NDELAY);
        if (!isatty(modem))
                return warn("Fatal: Device %s is not a tty.", ttypath);
        fcntl(modem, F_SETFL, 0);
        if (tcgetattr(modem, &tconf) < 0)
                return warn("Fatal: Failed to get termios config for %s\n", ttypath);

        // Set termios config
        tconf.c_cc[VTIME] = 0;
        tconf.c_cc[VMIN] = 1;
        tconf.c_oflag |= ONLCR;
        tconf.c_cflag |= CLOCAL;
        tconf.c_lflag |= ISIG;
        tconf.c_iflag &= ~(IXON | IXOFF | ISTRIP | IUCLC | INPCK);
        tconf.c_oflag &= ~(OLCUC | OFILL);
        tconf.c_cflag &= ~(CRTSCTS);
        tconf.c_lflag &= ~(XCASE | ECHO | ECHOE | ECHOCTL | ECHONL | ECHOK | ICANON);

        if (cfsetispeed(&tconf, B115200) < 0)
                return warn("Failed to set input speed\n");
        if (cfsetospeed(&tconf, B115200) < 0)
                return warn("Failed to set output speed\n");
        if (tcsetattr(modem, TCSANOW, &tconf) < 0)
                return warn("Failed to set termios config\n");

        tcflush(modem, TCIFLUSH);

        if (pthread_mutex_init(&modem_mx, NULL) < 0)
                return warn("Failed to initialize Modem Mutex");

        /* Set up config for init_cmds as defined in config.h */
        for (cp = init_cmds; cp->cmdname != NULL; cp++)
                if (modem_snprintf(rbuf, BUFFSZ, cp->timeout_ms, "%s", cp->req_fmt) < 0)
                        warn("Failed to send command for %s", cp->cmdname);
                else if (strstr(rbuf, "OK") == NULL)
                        warn("Failed to initialize %s | rbuf: %s", cp->cmdname, rbuf);
                else
                        notice("Initialized %s | rbuf: %s", cp->cmdname, rbuf);

        /* Setup server for Request Mode, i.e. interfacing commands */
        if (pthread_create(&reqth, NULL, rsrv_sockmux, SOCK_PATH_REQ) < 0)
                return warn("Failed to create Request Thread");
        if (pthread_detach(reqth) < 0)
                return warn("Failed to detach Request Thread");

        /* Setup Async thread for receiving asynchronous data */
        if (pthread_create(&asyncth, NULL, asrv_sockmux, SOCK_PATH_ASYNC) < 0)
                return warn("Failed to create Async Thread");
        if (pthread_detach(asyncth) < 0)
                return warn ("Failed to detach Async Thread");

        /* Clean up everything if we have a Ctrl-C on our hands */
        signal(SIGINT, atd_sigint);
        return 0;
}

int
atd_destroy()
{
        remove(SOCK_PATH_REQ);
        remove(SOCK_PATH_ASYNC);
        close(modem);
        if (pthread_mutex_destroy(&modem_mx) < 0)
                return warn("Failed to destroy Modem Mutex");
        if (pthread_mutex_destroy(&log_mx) < 0)
                return warn("Failed to destroy utility mutex");
        return 0;
}

int
atd_client_init(const char *path)
{
        int sock;
	//int i, n, bytes;
        struct sockaddr_un addr;
        char buf[BUFFSZ] = {0};
        socklen_t addrlen;
	if (!path)
		return -1;
        setlocale(LC_ALL, "");

        /* Set up UNIX address */
        addr.sun_family = AF_UNIX;
        strncpy(addr.sun_path, path, sizeof(addr.sun_path) - 1);
        addrlen = sizeof addr;

	/* Set up the socket connection */
        if ((sock = socket(AF_UNIX, SOCK_STREAM, 0)) < 0)
        	return warn("Unable to set up socket %s", path);
        if (connect(sock, (struct sockaddr*) &addr, addrlen) < 0)
        	return warn("Unable to connect to socket %s", path);

	/* Get the copyright notice or greeting from the server */
        if (read(sock, buf, BUFFSZ) < 0)
        	return warn("Unable to get greeting socket %s", path);

	notice("%s\n", buf);

	///* Send the command line arguments to the server */
	//memset(buf, 0, sizeof buf);
        //for (i = 0, n = BUFFSZ; i < argc && n > 1; ++i) {
        //        strncat(buf, argv[i], n);
        //        strcat(buf, " ");
        //        n -= strlen(argv[i]) + 1;
        //}
        //if ((bytes = write(sock, buf, BUFFSZ)) < 0)
        //        return warn("Failed to send command line arguments");
        //if ((unsigned) bytes < strlen(buf))
        //        return warn("Failed to send all the command line arguments");

        return sock;
}

int
atd_listen_mode(void *ex_data, ATC_HANDLER((*async_handler)))
{
        int sock, bytes;
        char rbuf[BUFFSZ] = {0}, *s;
        if ((sock = atd_client_init(SOCK_PATH_ASYNC)) < 0)
                return -1;
        while (1) {
		if (fdnprintf(sock, BUFFSZ, "heartbeat") < 0) {
	                warn("Failed to send heartbeat to file descriptor %d\n", sock);
                	close(sock);
                	return -1;
		}
		if ((bytes = fd_timeout_read(sock, rbuf, BUFFSZ, MODEM_TIMEOUT_MS/10)) < 0) {
	                warn("Failed to read from file descriptor %d\n", sock);
                	close(sock);
                	return -1;
		}
		if (!bytes)
			continue;
                if (!strncmp(rbuf, "quit", 4))
                        break;
		/* Clean the string of excessive newlines */
		while ((s = strstr(rbuf, "\n\n")))
			*s = ' ';
		/* Send the data to the async handler */
                async_handler(rbuf, strlen(rbuf), ex_data);
                /* Flush input buffer */
                memset(rbuf, 0, sizeof rbuf);
        }
        close(sock);
        return 0;
}


int
atd_cmd_mode(void *ex_data, ATC_HANDLER((*req_input_handler)), ATC_HANDLER((*req_output_handler)))
{
        int sock;
        char rbuf[BUFFSZ], tbuf[BUFFSZ], *s;
        if ((sock = atd_client_init(SOCK_PATH_REQ)) < 0)
                return -1;
        while (1) {
                memset(rbuf, 0, BUFFSZ * sizeof(char));
                memset(tbuf, 0, BUFFSZ * sizeof(char));
		/* Get the data from our input handler */
                req_input_handler(tbuf, BUFFSZ, ex_data);

		/* Push the data to the server */
                if (fdnprintf(sock, BUFFSZ, "%s", tbuf) < 0) {
                        warn("Failed to write to fd %d\n", sock);
                        close(sock);
                        return -1;
                }

                if (!strncmp(tbuf, "quit", 4))
                        break;

		/* Get the response from the server */
                if (read(sock, rbuf, BUFFSZ) < 0) {
                        warn("Failed to read from fd %d\n", sock);
                        close(sock);
                        return -1;
                }
		/* Clean the string of excessive newlines */
		while ((s = strstr(rbuf, "\n\n")))
			*s = ' ';
		/* Push the data to the output handler */
                req_output_handler(rbuf, strlen(rbuf) + 1, ex_data);
        }
        close(sock);
        return 0;
}

