# atd - the AT Daemon
# See LICENSE for copyright and license details.

include config.mk

HDR = util.h

PROG_SRC = main.c
LIB_SRC = atd.c

LIB_OBJ = ${LIB_SRC:.c=.o}
PROG_OBJ = ${PROG_SRC:.c=.o}

META = LICENSE Makefile config.def.h config.mk

all: options atd libatd.so

options:
	@echo "atd build options:"
	@echo "CC      = ${CC}"
	@echo "CFLAGS  = ${CFLAGS}"
	@echo "LDFLAGS = ${LDFLAGS}"

.c.o:
	${CC} -c ${CFLAGS} $<

${LIB_OBJ}: config.h config.mk
${PROG_OBJ}: config.h config.mk

config.h:
	cp config.def.h $@

libatd.so: ${LIB_OBJ}
	${CC} -shared -o $@ ${LIB_OBJ} ${LDFLAGS}

atd: ${PROG_OBJ} ${LIB_OBJ}
	${CC} -o $@ ${PROG_OBJ} ${LIB_OBJ} ${LDFLAGS}

clean:
	rm -f atd libatd.so ${PROG_OBJ} ${LIB_OBJ} atd-${VERSION}.tar.gz

dist: clean
	mkdir -p atd-${VERSION}
	cp -R ${META} ${LIB_SRC} ${PROG_SRC} atd-${VERSION} ${HDR}
	tar -cf atd-${VERSION}.tar
	rm -rf atd-${VERSION}

install: all
	mkdir -p ${DESTDIR}${PREFIX}/bin
	cp -f atd ${DESTDIR}${PREFIX}/bin
	chmod 755 ${DESTDIR}${PREFIX}/bin/atd
	mkdir -p ${DESTDIR}${PREFIX}/lib
	cp -f libatd.so ${DESTDIR}${PREFIX}/lib
	chmod 755 ${DESTDIR}${PREFIX}/lib/libatd.so
	ldconfig
	mkdir -p ${DESTDIR}${PREFIX}/include
	cp -f atd.h ${DESTDIR}${PREFIX}/include
	chmod 655 ${DESTDIR}${PREFIX}/include/atd.h

uninstall:
	rm -f ${DESTDIR}${PREFIX}/bin/atd ${DESTDIR}${PREFIX}/lib/libatd.so ${DESTDIR}${PREFIX}/include/atd.h
	ldconfig

.PHONY: all options clean dist install uninstall
