#ifndef __CONFIG_DEF_H__
#define __CONFIG_DEF_H__

static const char COPYRIGHT_MESSAGE[] =
    "atd - The Simple AT Daemon\n"
    "Copyright (C) 2020 Amber Katsu\n"
    "This program is free software, and comes with NO WARRANTY.\n"
    "See LICENSE for more details.\n";

/* Section 0: Custom Constants and Tweaks */
#define MXVSYSLOG(PRIORITY, FMT, ARGS) mxvsyslog((PRIORITY), (FMT), (ARGS))
#define TTY_PATH "/dev/ttyS0"
#define SOCK_PATH "/tmp/atd"
#define SOCK_PATH_REQ SOCK_PATH"/req.sock"
#define SOCK_PATH_ASYNC SOCK_PATH"/async.sock"
#define BUFFSZ 10000
#define MODEM_TIMEOUT_MS 350
#define MAX_NUM_ARGS 1000
#define MAX_NUM_CLIENTS 1000

struct init_cmd {
        const char *cmdname;
        const char *req_fmt;
        const unsigned int timeout_ms;
};
static const struct init_cmd init_cmds[] = {
    //{"Reset to Defaults", "AT&F\r\n",                             MODEM_TIMEOUT_MS * 10},
    {"Error message formatting", "AT+CMEE=2\r\n",                 MODEM_TIMEOUT_MS},
    {"Caller ID",         "AT+CLIP=1\r\n",                        MODEM_TIMEOUT_MS},
    {"Message Format",    "AT+CMGF=1\r\n",                        MODEM_TIMEOUT_MS},
    {"URC indicator",     "AT+QURCCFG=\"urcport\",\"uart1\"\r\n", MODEM_TIMEOUT_MS},
    {"Num of autorings",  "ATS0=0\r\n",                           MODEM_TIMEOUT_MS},
    {"Caller Fwding",     "AT+CCFC=0,4\r\n",                      MODEM_TIMEOUT_MS * 4},
    {0,0,0} /* NOTE TO HACKERS: REMEMBER TO KEEP THIS NULL TERMINATOR IN */
};

#endif
