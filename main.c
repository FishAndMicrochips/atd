#include <stdio.h>
#include <string.h>

#include "atd.h"
#include "util.h"

ATC_HANDLER(req_async_print);
ATC_HANDLER(req_gets);
void usage();

int
main(int argc, char **argv)
{
        ARGBEGIN {
        case 'd':
                if (atd_init() < 0)
                        return -1;
                getchar();
                if (atd_destroy() < 0)
                        return -1;
                return 0;
        case 'c':
                return atd_cmd_mode(NULL, req_gets, req_async_print);
        case 'l':
                return atd_listen_mode(NULL, req_async_print);
        default:
                usage();
        } ARGEND;
        usage();
        return -1;
}

ATC_HANDLER(req_async_print)
{
        ex_data = NULL;
        atd_buf_sz = 0;
        return printf("%s\n", atd_buf);
}

ATC_HANDLER(req_gets)
{
        ex_data = NULL;
        printf("ATC: ");
        fgets(atd_buf, atd_buf_sz, stdin);
        return strlen(atd_buf);
}

void
usage()
{
        fprintf(stderr,
                "Usage:\n"
                "\tatd -d\t- run as a daemon\n"
                "\tatd -c\t- Run as a client, sending AT commands and receiving a response from the modem.\n"
                "\t      \t  (NOTE: Must have an atd daemon active to run as client.)\n"
                "\tatd -l\t- Run as a listener, receiving asynchronous data coming from the modem.\n"
                "\t      \t  (NOTE: Must have an atd daemon active to run as listener.)\n");
        exit(EXIT_FAILURE);
}
